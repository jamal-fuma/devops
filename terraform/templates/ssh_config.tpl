Host ${web_alias}
  Hostname ${web_ip}
  User ${web_user}
  IdentityFile  ${identity_file}
  ProxyCommand ssh -W %h:%p ${jmp_alias}
  IdentitiesOnly yes

Host ${jmp_alias}
  Hostname ${jmp_ip}
  User ${jmp_user}
  ForwardAgent yes
  IdentityFile  ${identity_file}
  IdentitiesOnly yes
