variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-1"
}

variable "vpc_cidr" {
  default = "10.9.0.0/16"
}
variable "private_subnets" {
  default = ["10.9.0.0/24", "10.9.1.0/24"]
}
variable "public_subnets" {
  default = ["10.9.10.0/24", "10.9.11.0/24"]
}
variable "database_subnets" {
  default = ["10.9.20.0/24", "10.9.21.0/24"]
}
variable "sshprivkey_file_path" {
  description = "The path to the ssh private key"
}

variable "sshpubkey_file" {
  description = "The contents of the ssh public key"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "environment_tag" {
  default     = "dev"
  description = "The logical environment targeted by the current run of terraform files"
}

variable "author_tag" {
  description = "The github-id of the author of some modifications to terraform files"
}


# Tags required for all instances and subnets
locals {
  common_tags = {
    "env"    = var.environment_tag
    "author" = var.author_tag
  }
}
