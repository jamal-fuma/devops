data "aws_availability_zones" "zones" {
  state = "available"
}

# latest bastion filter
data "aws_ami" "latest_amazon_linux_ami" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2*"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

# latest nginx filter
data "aws_ami" "latest_nginx_ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = ["nginx*"]
  }

  filter {
    name   = "tag:Name"
    values = ["NGINX"]
  }

  filter {
    name   = "tag:Tier"
    values = ["WEB"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

# helper to make testing bastion easier
data "template_file" "ssh_config" {
  template = templatefile("templates/ssh_config.tpl", {
    web_alias     = "aws_web"
    web_ip        = aws_instance.web.private_ip
    web_user      = "ubuntu"
    jmp_alias     = "aws_bastion"
    jmp_ip        = aws_instance.bastion.public_ip
    jmp_user      = "ec2-user"
    identity_file = var.sshprivkey_file_path
  })
}

data "aws_vpcs" "all" {}
data "aws_vpc" "default" {
  default = true
}

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = data.aws_vpc.default.id
}
