module "vpc" {
  name   = "foo-vpc"
  source = "terraform-aws-modules/vpc/aws"

  cidr = var.vpc_cidr

  azs = [
    "${data.aws_availability_zones.zones.names[0]}",
    "${data.aws_availability_zones.zones.names[1]}",
  ]

  private_subnets  = var.private_subnets
  public_subnets   = var.public_subnets
  database_subnets = var.database_subnets

  create_database_subnet_group = false

  enable_dns_hostnames = true
  enable_dns_support   = true
  single_nat_gateway   = true
  enable_nat_gateway   = true
  enable_s3_endpoint   = true
  tags                 = local.common_tags

  public_subnet_tags = {
    Role = "public"
  }

  private_subnet_tags = {
    Role = "private"
  }

  public_route_table_tags = {
    Name = "public-RT"
  }

  private_route_table_tags = {
    Name = "private-RT"
  }
}
