resource "aws_instance" "bastion" {
  associate_public_ip_address = true
  instance_type               = var.instance_type
  vpc_security_group_ids      = [aws_security_group.bastion-sg.id]
  subnet_id                   = module.vpc.public_subnets[0]
  key_name                    = aws_key_pair.operator.key_name
  ami                         = data.aws_ami.latest_amazon_linux_ami.id
  tags = {
    "name"   = "bastion"
    "env"    = var.environment_tag
    "author" = var.author_tag
  }
}

resource "aws_instance" "web" {
  associate_public_ip_address = true
  instance_type               = var.instance_type
  vpc_security_group_ids      = [aws_security_group.web-sg.id]
  subnet_id                   = module.vpc.public_subnets[0]
  key_name                    = aws_key_pair.operator.key_name
  ami                         = data.aws_ami.latest_nginx_ami.id
  tags = {
    "name"   = "nginx"
    "env"    = var.environment_tag
    "author" = var.author_tag
  }
}
