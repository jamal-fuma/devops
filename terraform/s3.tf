resource "aws_s3_bucket" "datastore" {
  bucket_prefix = "app-datastore"
  acl           = "private"
  region        = var.region
  tags          = local.common_tags
}
