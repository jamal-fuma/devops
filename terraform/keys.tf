resource "aws_key_pair" "operator" {
  key_name   = "operator-key"
  public_key = var.sshpubkey_file
}
