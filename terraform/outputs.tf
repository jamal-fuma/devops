output "ssh_config" {
  value = "${data.template_file.ssh_config.rendered}"
}

output "bastion_public_ip" {
  value = "${aws_instance.bastion.public_ip}"
}

output "web_public_ip" {
  value = "${aws_instance.web.public_ip}"
}

output "web_private_ip" {
  value = "${aws_instance.web.private_ip}"
}

output "s3_bucket_name" {
  value = "${aws_s3_bucket.datastore.bucket}"
}

output "operator_public_key" {
  value = "${aws_key_pair.operator.public_key}"
}

