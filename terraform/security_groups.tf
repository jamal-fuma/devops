resource "aws_security_group" "bastion-sg" {
  name_prefix = "bastion-sg"
  description = "Security Group for Bastion tier"
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    "author" = var.author_tag
    "env"    = var.environment_tag
    "name"   = "Bastion-SG"
  }
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "allow_all_incoming_ssh_for_bastion" {
  description       = "ACCEPT SSH from ALL - Bastion"
  security_group_id = aws_security_group.bastion-sg.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = ["0.0.0.0/0"]
  protocol          = "tcp"
}

resource "aws_security_group_rule" "allow_all_outgoing_for_bastion" {
  description       = "ACCEPT ALL to ALL - Bastion"
  security_group_id = aws_security_group.bastion-sg.id
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  cidr_blocks       = ["0.0.0.0/0"]
  protocol          = "-1" # all
}

resource "aws_security_group" "web-sg" {
  name_prefix = "web-sg"
  description = "Security Group for Web tier"
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    "author" = var.author_tag
    "env"    = var.environment_tag
    "name"   = "Web-SG"
  }
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "allow_all_http_for_web" {
  description       = "ACCEPT HTTP from ALL - Web"
  security_group_id = aws_security_group.web-sg.id
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["0.0.0.0/0"]
  protocol          = "tcp"
}

resource "aws_security_group_rule" "allow_bastion_ssh_for_web" {
  description       = "ACCEPT SSH from BastionSG - Web"
  security_group_id = aws_security_group.web-sg.id
  source_security_group_id = aws_security_group.bastion-sg.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
}

resource "aws_security_group_rule" "allow_all_outgoing_for_web" {
  description       = "ACCEPT ALL to ALL - Web"
  security_group_id = aws_security_group.web-sg.id
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  cidr_blocks       = ["0.0.0.0/0"]
  protocol          = "-1" # all
}
