### Terraform

A minimal vpc with bastion host is created with a s3 bucket + endpoint

nginx is running in the public subnet.
The nginx ami is latest tagged with
- "Name": "NGINX",
- "Tier": "WEB",
- "NotBefore": "{{ isotime }}",
- "TimestampUTC": "{{ timestamp }}"


The bastion ami is latest amazon_linux

Connection details are output at the end of the run

### Variables
These are expected to be present in the environment when running terraform
- TF_VAR_author_tag
- TF_VAR_access_key
- TF_VAR_secret_key

export  TF_LOG TF_LOG_PATH \
        TF_VAR_author_tag \
        TF_VAR_access_key \
        TF_VAR_secret_key

exec terraform "$@"

### Running Terraform
The following script will use aws credentials from ~/.aws/credentials
save it in your path as terraformw.

The run packer as terraformw and the configuration will be populated into the environment

```
#!/bin/sh
TF_LOG="TRACE"
TF_LOG_PATH="run.log"

TF_VAR_access_key="$(sed -n '/^aws_access_key_id[^=]*=[ ]\+\(.*\)$/{s//\1/; p;}' ~/.aws/credentials)"
TF_VAR_secret_key="$(sed -n '/^aws_secret_access_key[^=]*=[ ]\+\(.*\)$/{s//\1/; p;}' ~/.aws/credentials)"

export  TF_LOG TF_LOG_PATH \
        TF_VAR_access_key \
        TF_VAR_secret_key

exec /home/jamal/bin/terraform "$@"
```
