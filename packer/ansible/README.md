### Ansible

A minimal ubuntu flavour set of playbooks for use with packer.

##### bootstrap-python.yml
    Ansible required python so this playbook needs running first

##### repair.yml
    Remove all the issues located in ami

##### networking.yml
    Apply firewall policy

##### apt.yml
    Install required software

##### nginx.yml
    Configure nginx startup

##### limits.yml
    Enforce system limits

#### templates
    Helpers scripts

#### vars
    Additional configuration for playbooks

##### repair.yml
    - user_paths

##### apt.yml
    - required_packages
    - auto_upgrade_services
    - auto_upgrade_configuration

##### bootstrap.yml
    - bootstrap_environment

##### limits.yml
    - hard_limit_on_open_files
    - soft_limit_on_open_files
    - sysctl_limit_values
    - security_limit_users
    - security_limits

##### networking.yml
    - internet_protocol_versions
    - chain_policy
    - opened_ports
