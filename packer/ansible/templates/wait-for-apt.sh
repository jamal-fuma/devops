#!/bin/sh

exec /usr/bin/systemd-run \
    --property="After=apt-daily.service apt-daily-upgrade.service" \
    --wait /bin/true
