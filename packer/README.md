### Packer

A minimal ebs-builder with configuration taken from environment

### Variables
These are expected to be present in the environment whem running packer

- base_ami_id`
- ami_name_prefix`
- instance_type`
- access_key`
- secret_key`
- region`
- ssh_user`

### Builders
A single builder is support, the ebs-builder on AWS

The resulting ami is tagged with
- "Name": "NGINX",
- "Tier": "WEB",
- "NotBefore": "{{ isotime }}",
- "TimestampUTC": "{{ timestamp }}"

### Provisioners
Ansible is used to provision the images, the playbooks are executed in order
See the documentation in ansible/README.md for details on the playbooks


### Running Packer
The following script will use aws credentials from ~/.aws/credentials
save it in your path as packerw.

The run packer as packerw and the configuration will be populated into the environment

```
#!/bin/sh

PACKER_LOG="TRACE"
PACKER_LOG_PATH="run.log"

access_key="$(sed -n '/^aws_access_key_id[^=]*=[ ]\+\(.*\)$/{s//\1/; p;}' ~/.aws/credentials)"
secret_key="$(sed -n '/^aws_secret_access_key[^=]*=[ ]\+\(.*\)$/{s//\1/; p;}' ~/.aws/credentials)"

region="eu-west-1"
base_ami_id="ami-046dd95b1255a4a03"
instance_type="t2.micro"
ami_name_prefix="nginx"
ssh_user="ubuntu"

export  PACKER_LOG PACKER_LOG_PATH \
        region \
        base_ami_id \
        instance_type \
        ami_name_prefix \
        author_tag \
        sshpubkey_file \
        ssh_user \
        access_key \
        secret_key

exec packer "$@"

```
