#!/usr/bin/env python3
import os
import json
from app import global_configuration
from app import S3Transfer
from flask import Flask, request, jsonify, Response

app = Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def form_handler():
    with open(os.path.join(app.root_path, 'www/upload.html'), 'rt') as f:
        return str(f.read()), 200


@app.route('/app.js', methods=['GET'])
def js_handler():
    with open(os.path.join(app.root_path, 'www/app.js'), 'rt') as f:
        body = str(f.read())
        return Response(body, mimetype="text/javascript", status=200)


@app.errorhandler(404)
def page_not_found(e):
    with open(os.path.join(app.root_path, 'www/404.html'), 'rt') as f:
        return str(f.read()), 404


@app.route('/download/<object_id>', methods=['GET'])
def download_handler(object_id):
    client = S3Transfer()
    rv = client.download(object_id)
    return Response(rv.data, mimetype=rv.headers["ContentType"],
                    status=rv.status_code)


@app.route('/upload', methods=['POST'])
def upload_handler():
    declared_mime_type = 'application/octet-stream'
    if( request.headers.get('X-Application-ContentType')):
        declared_mime_type = request.headers['X-Application-ContentType']

    client = S3Transfer()
    rv = client.upload(
        request.data,
        declared_mime_type)
    return Response(rv.data, mimetype="application/json",
                    status=rv.status_code)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5001')
