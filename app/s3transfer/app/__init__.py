#!/usr/bin/env python3
import os
import json
import hashlib
import boto3
import base64


def global_configuration():
    config = {
        "bucket": os.environ.get('PY_S3_bucket'),
        "access_key": os.environ.get('PY_S3_access_key'),
        "secret_key": os.environ.get('PY_S3_secret_key')
    }
    return json.dumps(config)


def set_connection_header(request, operation_name, **kwargs):
    request.headers['Connection'] = 'close'


class Response(object):
    def __init__(self, code, data, headers={}):
        self.__dict__["status_code"] = code
        self.__dict__["data"] = data
        self.__dict__["headers"] = headers


class S3Transfer(object):
    def __init__(self):
        self.conf = json.loads(global_configuration())
        self.s3 = boto3.client(
            "s3",
            aws_access_key_id=self.conf["access_key"],
            aws_secret_access_key=self.conf["secret_key"],
            region_name="eu-west-2")
        self.s3.meta.events.register(
            'request-created.s3', set_connection_header)

    def name_str(self, data):
        return hashlib.sha256(self.encode_inner(data)).hexdigest()

    def encode_inner(self, data):
        try:
            payload = bytes(data, 'utf-8')
            return payload
        except BaseException:
            return data

    def upload(self, data, mime_type):
        try:
            object_id = self.name_str(data)
            data_bytes = self.encode_inner(data)
            rsp = self.s3.put_object(
                Body=data_bytes,
                Bucket=self.conf["bucket"],
                Key=object_id,
                ContentType=mime_type)
            print("Got bytes: {}".format(len(data_bytes)))
            return Response(201, json.dumps({"object_id": object_id}))
        except Exception as e:
            print(e)
            return Response(403, "Computer says no '{}'".format(e))

    def download(self, object_id):
        try:
            rsp = self.s3.get_object(
                Bucket=self.conf["bucket"],
                Key=object_id)
            return Response(200, rsp["Body"].read(),{'ContentType': rsp["ContentType"]})
        except Exception as e:
            print(e)
            if e.response['Error']['Code'] == 'NoSuchKey':
                return Response(
                    404, "No object with key {} found".format(object_id))
            return Response(403, "Computer says no '{}'".format(e))
