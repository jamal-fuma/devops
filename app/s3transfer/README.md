# S3Transfer
Simple S3 service using docker for deployment

simple client ui in js is provided running on port 5000

### UI
open 'http://127.0.0.1:5000/' in a browser


### Configuration
Configuration is taken from the environment
```
PY_S3_bucket
PY_S3_access_key
PY_S3_secret_key
```

#### Running Application in Docker
The following script will use aws credentials from ~/.aws/credentials
and accept a bucket name from the environment

save it in your path as pywrapper

The run as pywrapper and the configuration will be populated into the environment

```
#!/bin/sh

PY_S3_access_key="$(sed -n '/^aws_access_key_id[^=]*=[ ]\+\(.*\)$/{s//\1/; p;}' ~/.aws/credentials)"
PY_S3_secret_key="$(sed -n '/^aws_secret_access_key[^=]*=[ ]\+\(.*\)$/{s//\1/; p;}' ~/.aws/credentials)"
PY_S3_bucket=${PY_S3_bucket:-'missing'}

if [ "x$PY_S3_bucket" = "xmissing" ]; then
    printf "Error: %s %s\n" "Unset environment variable PY_S3_bucket" "Please export PY_S3_bucket with name of S3 Bucket created by terraform";
    exit `false`;
fi

export  PY_S3_access_key \
        PY_S3_secret_key \
        PY_S3_bucket

docker build --rm -t vnova-py-s3transfer .

exec docker run \
-e "PY_S3_access_key=${PY_S3_access_key}" \
-e "PY_S3_secret_key=${PY_S3_secret_key}" \
-e "PY_S3_bucket=${PY_S3_bucket}" \
-p 5000:5001 \
-it \
vnova-py-s3transfer
```


##  Usage
Support two endpoints
- POST /upload -> returns { "object\_id", object\_id }
- GET /download/object\_id -> download a previously uploaded object

### Example usage with curl and jq
We need to remember the declared mime type so an additional header is used.
If this header is not specified the content type will be stored as 'application/octet-stream'

With 'X-Application-ContentType'

```
 curl \
    -qsq \
    -X POST \
    -H 'Content-Type: application/json' \
    -H 'X-Application-ContentType: application/json' \
    'http://127.0.0.1:5000/upload' \
    -d "$(\
jq \
    -n \
    -r \
    -c '{"hello": $hello}' \
    --arg hello world \
)" \
| jq \
    '"http://localhost:5000/download/\(.object_id)"' \
    | xargs \
    curl \
    -D- \
    -Ssl \
    -qs
```
### Example Output
```
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 17
Server: Werkzeug/1.0.0 Python/3.8.1
Date: Sun, 23 Feb 2020 18:57:53 GMT

{"hello":"world"}
```
