var application_js = function(){
    'use strict';

    // helpers
    var networking = {
        base_url: "http://127.0.0.1:5000",
        // dry up the boilerplate of api calls
        make_fetcher: function(on_complete){
            'use strict';
            var content = {
                mime_type: "",
                data: undefined,
                length: 0,
                is_json: false
            };
            var reader = new FileReader;
            reader.addEventListener('load', function(){
                'use strict';
                content.data = reader.result;
                on_complete(content);
            });
            content.fetch = function(api_url,api_opt){
                'use strict';
                fetch(api_url,api_opt)
                    .then(function(response){
                        if(!response.ok)
                            return new Error('Response is not okay');
                        return response.blob();
                    },function(err){ console.error(err.toString()); })
                    .then(function(blob){
                        content.mime_type =  blob.type;
                        content.length = blob.size;
                        content.is_json  = blob.type.includes( 'application/json' );
                        reader.readAsDataURL(blob);
                    },function(err){ console.error(err.toString()); })
                    .catch(function(err){ console.error(err.toString()); });
            };
            return content;
        }
    };

    // api front ends
    var services = {
        upload_file: function(blob,on_complete){
            'use strict';
            console.log("uploading file of type: " + blob.type);
            var api_url = [networking.base_url,'upload'].join('/');
            var api_opt = {body: blob, method:   'POST', headers: {"Connection": "close", "X-Application-ContentType": blob.type}};
            return networking.make_fetcher(on_complete).fetch(api_url,api_opt);
        },

        download_with_object_id: function(object_id,on_complete){
            'use strict';
            var api_url = [networking.base_url,'download',object_id].join('/');
            var api_opt = { method:   'GET', headers: {"Connection": "close"}};
            return networking.make_fetcher(on_complete).fetch(api_url,api_opt);
        }
    };

    var view_model = {
        content_frame: document.querySelector('#frameContent'),
        nav_list: document.querySelector('#ulNav'),
        upload_form: document.querySelector('#frmUpload'),
        upload_form_button: document.querySelector('#btnUpload'),
        upload_file_field: document.querySelector('#uploadable_file'),
        download_form:  document.querySelector('#frmDownload'),
        download_form_button: document.querySelector('#btnDownload'),
        object_id_field: document.querySelector('#object_id'),
    };

    view_model.add_menu_item = function(name,object_id,content_url){
        if(document.querySelector('#li-' + object_id))
            return;

        var a = document.createElement('a');
        a.setAttribute('href',content_url);
        a.setAttribute('download',name);
        //a.setAttribute('target',view_model.frameContent.getAttribute('name'));
        a.setAttribute('target',"_blank");
        a.setAttribute('data-object-id',object_id);
        a.textContent = name;
        a.addEventListener('click',function(event){
            'use strict';
            event.preventDefault();
            view_model.object_id_field.value = object_id;
            view_model.download_form_button.click();
        });

        var li = document.createElement('li');
        li.appendChild(a);
        li.id = ['li',object_id].join('-');
        view_model.nav_list.appendChild(li);
    };

    var data_model = {
        objects: {},
        total_bytes: 0,
    };
    data_model.on_upload_complete = function(fname,object_id){
        'use strict';
        data_model.objects[object_id] = {id: object_id, name: fname, size:0};
        var msg = ["uploaded:",fname,"with object_id:",object_id].join(' ');
        console.log(msg);
    };
    data_model.on_download_complete = function(object_id,response_size){
        'use strict';
        data_model.total_bytes += response_size;
        data_model.objects[object_id].size = response_size;
        var fname = data_model.objects[object_id].name;
        var msg = ["downloaded:",fname,"with object_id:",object_id,"was size in bytes:",response_size].join(' ');
        console.log(msg);
    };
    // mvp
    var presenter = {
        // handle download with plain get to api
        download_clicked: function(event){
            'use strict';
            event.preventDefault();
            var object_id = view_model.object_id_field.value;
            if(object_id.length ===0)
            {
                console.error("Unable to download as object_id empty");
                return false;
            }
            services.download_with_object_id(object_id,function(content){
                'use strict';
                data_model.on_download_complete(object_id,content.length);
                view_model.content_frame.src = content.data;
                view_model.object_id_field.value = "";
            });
        },

        // handle upload with plain post to api
        upload_clicked: function(event){
            'use strict';
            event.preventDefault();
            var blob = view_model.upload_file_field.files[0];
            var fname = view_model.upload_file_field.value.replace(/^.*(\\|\/|\:)/, '');
            services.upload_file(blob,function(content){
                'use strict';
                var data_str = atob(content.data.split(',')[1]);
                var data = JSON.parse(data_str);
                data_model.on_upload_complete(fname,data.object_id);
                view_model.upload_file_field.value = "";
                view_model.object_id_field.value = data.object_id;
                view_model.add_menu_item(fname,data.object_id,content.data);
            });
        },

        // handle form submissions manually
        register_event_handlers: function(){
            'use strict';
            view_model.upload_form.addEventListener('submit',this.upload_clicked);
            view_model.upload_form_button.addEventListener('click',this.upload_clicked);
            view_model.upload_file_field.addEventListener('change',this.upload_clicked);
            view_model.download_form.addEventListener('submit',this.download_clicked);
            view_model.object_id_field.addEventListener('click',this.download_clicked);
            view_model.download_form_button.addEventListener('click',this.download_clicked);
        }
    };

    presenter.register_event_handlers();

    return this;
};

// load all this stuff when ready
var g_app = null;

if ( document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll))
{
    g_app = application_js();
}
else
{
    document.addEventListener("DOMContentLoaded", function(){ g_app = application_js(); });
}
