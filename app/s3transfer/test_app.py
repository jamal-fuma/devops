#!/usr/bin/env python3
import unittest
import os
import json
from app import global_configuration
from app import S3Transfer, Response


class S3TransferTests(unittest.TestCase):
    FIXTURE_HASH = "5f8f04f6a3a892aaabbddb6cf273894493773960d4a325b105fee46eef4304f1"

    def setUp(self):
        self.data = {"hello": "world"}
        self.resp = {"object_id": 2}

    def tearDown(self):
        pass

    def test_global_config_pulls_bucket_from_env(self):
        old_val = os.environ["PY_S3_bucket"]
        os.environ["PY_S3_bucket"] = "bucket1"
        conf = json.loads(global_configuration())
        self.assertEqual(conf["bucket"], "bucket1")
        os.environ["PY_S3_bucket"] = old_val

    def test_global_config_pulls_access_key_from_env(self):
        old_val = os.environ["PY_S3_access_key"]
        os.environ["PY_S3_access_key"] = "access_key"
        conf = json.loads(global_configuration())
        self.assertEqual(conf["access_key"], "access_key")
        os.environ["PY_S3_access_key"] = old_val

    def test_global_config_pulls_secret_key_from_env(self):
        old_val = os.environ["PY_S3_secret_key"]
        os.environ["PY_S3_secret_key"] = "secret_key"
        conf = json.loads(global_configuration())
        self.assertEqual(conf["secret_key"], "secret_key")
        os.environ["PY_S3_secret_key"] = old_val

    def test_fake_response(self):
        rv = Response(1, "foo")
        self.assertEqual(rv.status_code, 1)
        self.assertEqual(rv.data, "foo")

    def test_upload_should_use_content_addressable_storage(self):
        actual = S3Transfer().name_str(json.dumps(self.data))
        self.assertEqual(self.FIXTURE_HASH, actual)
        return actual

    def test_upload_should_return_201_on_success(self):
        rv = S3Transfer().upload(json.dumps(self.data),'application/json')
        self.assertEqual(rv.status_code, 201)
        return rv

    def test_upload_should_return_403_on_error(self):
        old_sec = os.environ["PY_S3_secret_key"]
        old_pub = os.environ["PY_S3_access_key"]
        os.environ["PY_S3_secret_key"] = ""
        os.environ["PY_S3_access_key"] = ""
        conf = json.loads(global_configuration())
        self.assertEqual(conf["secret_key"], "")
        self.assertEqual(conf["access_key"], "")
        rv = S3Transfer().upload(json.dumps(self.data),'application/json')
        self.assertEqual(rv.status_code, 403)
        self.assertEqual(
            rv.data,
            "Computer says no 'An error occurred (AuthorizationHeaderMalformed) when calling the PutObject operation: The authorization header is malformed; a non-empty Access Key (AKID) must be provided in the credential.'")
        os.environ["PY_S3_secret_key"] = old_sec
        os.environ["PY_S3_access_key"] = old_pub
        return rv

    def test_upload_should_return_id_on_success(self):
        rv = self.test_upload_should_return_201_on_success()
        result = json.loads(rv.data)
        self.assertEqual(result["object_id"], self.FIXTURE_HASH)
        return rv

    def test_download_should_return_404_when_id_not_found(self):
        rv = S3Transfer().download("1")
        self.assertEqual(rv.status_code, 404)
        self.assertEqual(rv.data, "No object with key 1 found")
        return rv

    def test_download_should_return_403_on_error(self):
        old_sec = os.environ["PY_S3_secret_key"]
        old_pub = os.environ["PY_S3_access_key"]
        os.environ["PY_S3_secret_key"] = ""
        os.environ["PY_S3_access_key"] = ""
        conf = json.loads(global_configuration())
        self.assertEqual(conf["secret_key"], "")
        self.assertEqual(conf["access_key"], "")
        rv = S3Transfer().upload(json.dumps(self.data),'application/json')
        self.assertEqual(rv.status_code, 403)
        self.assertEqual(
            rv.data,
            "Computer says no 'An error occurred (AuthorizationHeaderMalformed) when calling the PutObject operation: The authorization header is malformed; a non-empty Access Key (AKID) must be provided in the credential.'")
        os.environ["PY_S3_secret_key"] = old_sec
        os.environ["PY_S3_access_key"] = old_pub
        return rv

    def test_download_should_return_200_when_id_found(self):
        rv = S3Transfer().download(self.FIXTURE_HASH)
        self.assertEqual(rv.status_code, 200)
        return rv

    def test_download_should_return_payload_when_id_found(self):
        rv = self.test_download_should_return_200_when_id_found()
        result = json.loads(rv.data)
        self.assertEqual(result, self.data)
        return rv


if __name__ == "__main__":
    unittest.main()
